/**
 * 
 */
package com.app.service;

import com.app.vo.Temperature;

/**
 * @author pbwag
 *
 */
public interface WeatherService {
	
	public Temperature getColdestHourByZip(String zip);

	Double[] findLatLong(String zip);
}
