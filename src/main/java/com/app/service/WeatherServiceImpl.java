/**
 * 
 */
package com.app.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.vo.Temperature;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.GetRequest;

/**
 * @author pbwag
 *
 */
@Service
public class WeatherServiceImpl implements WeatherService {

	@Autowired
	WeatherService weatherService;

	public WeatherServiceImpl() {

		Unirest.setObjectMapper(new ObjectMapper() {

			com.fasterxml.jackson.databind.ObjectMapper mapper = new com.fasterxml.jackson.databind.ObjectMapper();

			public String writeValue(Object value) {
				try {
					return mapper.writeValueAsString(value);
				} catch (JsonProcessingException e) {
					System.out.println(e);
				}

				return "";
			}

			public <T> T readValue(String value, Class<T> valueType) {
				try {
					return mapper.readValue(value, valueType);
				} catch (IOException e) {
					System.out.println(e);
				}

				return null;
			}
		});

		Unirest.setTimeouts(5000, 5000);
	}

	@Override
	public Temperature getColdestHourByZip(String zip) {

		Double[] latlong = weatherService.findLatLong(zip + ",US");

		GetRequest jsonResponse = Unirest.get("https://api.openweathermap.org/data/2.5/onecall?lat=" + latlong[0]
				+ "&lon=" + latlong[1] + "&appid=04bcc2b944b7ea03949f57bc0627c2cd")
				.header("Content-Type", "application/json");

		try {

			HttpResponse<String> resp = jsonResponse.asString();

			if (resp.getStatus() == 200) {

				JsonObject json = new Gson().fromJson(resp.getBody(), JsonObject.class);
				JsonArray hourly = json.getAsJsonArray("hourly");
				List<Temperature> tList = new ArrayList<>();
				DateTime start = new DateTime(DateTimeZone.forID(json.get("timezone").getAsString())).plusDays(1)
						.withTime(0, 0, 0, 0);
				DateTime end = new DateTime(DateTimeZone.forID(json.get("timezone").getAsString())).plusDays(1)
						.withTime(23, 59, 59, 999);

				for (JsonElement e : hourly) {

					JsonObject hourObj = e.getAsJsonObject();
					Temperature temp = new Temperature(hourObj.get("dt").getAsLong() * 1000,
							convertKelvinToCelsius(hourObj.get("temp").getAsFloat()),
							json.get("timezone").getAsString());
					DateTime dateTime = new DateTime(hourObj.get("dt").getAsLong() * 1000,
							DateTimeZone.forID(json.get("timezone").getAsString()));

					if (dateTime.isAfter(start) && dateTime.isBefore(end)) {
						tList.add(temp);
					}
				}

				List<Temperature> nList = tList.stream().sorted(Comparator.comparing(Temperature::getTemp))
						.collect(Collectors.toList());

				return nList.get(0);

			}

		} catch (UnirestException e) {
			System.out.println(e);
		}

		return null;
	}

	@Override
	public Double[] findLatLong(String zip) {

		GetRequest jsonResponse = Unirest
				.get("http://api.positionstack.com/v1/forward?access_key=9a8ea9b347068cf6e9aff2dc80fbca41&query=" + zip)
				.header("Content-Type", "application/json");
		try {

			HttpResponse<String> resp = jsonResponse.asString();

			if (resp.getStatus() == 200) {

				JsonObject j = new Gson().fromJson(resp.getBody(), JsonObject.class);
				JsonArray json = j.getAsJsonArray("data").getAsJsonArray();

				if (json.size() > 0) {

					Double[] d = new Double[2];
					d[0] = json.get(0).getAsJsonObject().get("latitude").getAsDouble();
					d[1] = json.get(0).getAsJsonObject().get("longitude").getAsDouble();

					return d;
				}
			}

		} catch (UnirestException e) {
			System.out.println(e);
		}

		return null;
	}

	private static float convertKelvinToCelsius(float kelvin) {
		return (float) (kelvin - 273.15);
	}

}
