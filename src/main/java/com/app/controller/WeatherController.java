/**
 * 
 */
package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.service.WeatherService;
import com.app.vo.Temperature;

/**
 * @author pbwag
 *
 */
@RestController
public class WeatherController {

	@Autowired
	WeatherService weatherService;

	@RequestMapping("/getColdestHourByZip/{zip}")
	public Temperature getColdestHourByZip(@PathVariable("zip") String zip) {
		return weatherService.getColdestHourByZip(zip);
	}

}
