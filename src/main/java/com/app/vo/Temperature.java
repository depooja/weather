/**
 * 
 */
package com.app.vo;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/**
 * @author pbwag
 *
 */
public class Temperature {

	private Long date;
	private Float temp;
	private String timeZone;
	private String stringDate;

	public Temperature() {
	}

	public Temperature(Long date, Float temp, String timeZone) {
		this.date = date;
		this.temp = temp;
		this.timeZone = timeZone;
		this.stringDate = new DateTime(date, DateTimeZone.forID(timeZone)).toString();
	}

	public Long getDate() {
		return date;
	}

	public void setDate(Long date) {
		this.date = date;
	}

	public Float getTemp() {
		return temp;
	}

	public void setTemp(Float temp) {
		this.temp = temp;
	}

	public String gettimeZone() {
		return timeZone;
	}

	public void setTz(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getStringDate() {
		return stringDate;
	}

	public void setStringDate(String stringDate) {
		this.stringDate = stringDate;
	}

	@Override
	public String toString() {
		return "Temperature [date=" + new DateTime(date, DateTimeZone.forID(timeZone)) + ", temp=" + temp + "]";
	}
}
