package com.app.weather;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.app.controller.WeatherController;

@SpringBootTest
class WeatherApplicationTests {

	@Autowired
	private WeatherController weatherController;
	
	@Test
	void contextLoads() throws Exception {
		Assert.assertNotNull(weatherController);
	}

}
